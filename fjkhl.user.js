// ==UserScript==
// @name        flashscore - j/k/l/h navigation
// @namespace   oyvey
// @description Adds some navigation to FlashScore
// @include     https://www.flashscore.com*
// @homepage    https://bitbucket.org/dsjkvf/userscript-flashscore-jkhl-navigation
// @downloadURL https://bitbucket.org/dsjkvf/userscript-flashscore-jkhl-navigation/raw/master/fjkhl.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-flashscore-jkhl-navigation/raw/master/fjkhl.user.js
// @version     1.1.0
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @icon        https://www.flashscore.com/res/image/mobile-icons/fs-black/180x180.png
// @icon64      https://www.flashscore.com/res/image/mobile-icons/fs-black/180x180.png
// @run-at      document-end
// @grant       none
// ==/UserScript==

// INIT

var curPos = 0,
    articleDetails = [],
    offsets = [];

// HELPERS

function fillOffsets() {
    /* removes crap, fills the offsets table
    */
    scrollTo(0, 0);

    var elements = document.getElementsByTagName('thead');

    function getOffset(i) {
        var rxLeague = /.*tr class="league ._._(.+?) .*/;
        return offsets[i] || (( offsets[i] = [elements[i].getBoundingClientRect().top, elements[i].innerHTML.replace(rxLeague, "$1")] ));
    };
    var i = 0;
    while (i < elements.length) {
        getOffset(i);
        i++;
    };
};

function getPos(el, arrayElements) {
    /* returns a pair of articles that surround the current scroll position
    */

    var i = 0;
    if (el < arrayElements[0][0]) {
        return [0, arrayElements[0]];
    }
    while (i < arrayElements.length) {
        if (arrayElements[i][0] < el && el <= arrayElements[i+1][0]) {
            return [arrayElements[i], arrayElements[i+1]];
        };
        i++;
    };
};

function toggleCollapse(id) {
    /* toggles collapsed or expanded article's state by article's unique id
    */
    var chapters = document.getElementsByClassName("expand-collapse-icon");
    var i = 0;
    while (i < chapters.length) {
        if (chapters[i].innerHTML.indexOf(id) != -1 ) {
            if ( chapters[i].innerHTML.indexOf("collapse-league") !== -1 ) {
                chapters[i].getElementsByClassName("collapse-league")[0].click();
            } else {
                chapters[i].getElementsByClassName("expand-league")[0].click();
            };
        };
        i++
    };
};

function toggleCollapseAll(mode) {
    /* toggles collapsed or expanded article's state for all artiles
    */
    if (confirm("Sure?")) {
        var chapters = document.getElementsByClassName("expand-collapse-icon");
        if (mode == "collapse") {
            for (var j = 0; j < chapters.length; j++) {
                if ( chapters[j].innerHTML.indexOf("collapse-league") !== -1 ) {
                    chapters[j].getElementsByClassName("collapse-league")[0].click();
                };
            };
        } else if (mode == "expand") {
            for (var i = 0; i < chapters.length; i++) {
                if ( chapters[i].innerHTML.indexOf("expand-league") !== -1 ) {
                    chapters[i].getElementsByClassName("expand-league")[0].click();
                };
            };
        };
    };
};

function toArticle(scroll, down, index, delta) {
    /* scrolls (true), or collapses the selected league;
       scrolls either down (true) or up (false);
       takes coordinates from either lower, or upper neighboring article;
       adds either positive, or negative delta (for scrolling purposes)
    */

    curPos = document.body.scrollTop + delta;
    if (down) {
        offsets = [];
        fillOffsets();
    };
    articleDetails = getPos(curPos, offsets);
    if (scroll) {
        scrollTo(0, articleDetails[index][0] - 1);
    } else {
        toggleCollapse(articleDetails[index][1]);
        offsets = [];
        fillOffsets();
        scrollTo(0, curPos);
    }
};

function toTab(theTab) {
    /* emulates clicking a link containing the provided text
    */
    scrollTo(0, 0);
    var switchLinks = $("a:contains("+theTab+")")
    var evt = document.createEvent ("HTMLEvents");
    evt.initEvent ("click", true, true);
    switchLinks[0].dispatchEvent (evt);
};

// MAIN

addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 72 /*h*/ && e.which !== 74 /*j*/ && e.which !== 75 /*k*/ && e.which !== 76 /*l*/ &&
         e.which !== 84 /*t*/ && e.which !== 81 /*q*/ && e.which !== 87 /*w*/ &&
         e.which !== 88 /*x*/ )) {
        return;
    }
    e.preventDefault();

    if (e.which == 88) {
        offsets = [];
        fillOffsets();
    } else if (e.which == 74) {
        // scrolls down, re-fills the offsets table
        toArticle(true, true, 1, 2);
    } else if (e.which == 75) {
        // scrolls up, doesn't re-fill the offsets table
        toArticle(true, false, 0, -2);
    } else if (e.which == 84) {
        toArticle(false, false, 1, 0);
    } else if (e.which == 81) {
        toggleCollapseAll("collapse");
    } else if (e.which == 87) {
        toggleCollapseAll("expand");
    } else {
        var selCode = document.querySelector('ul.ifmenu').innerHTML;
        if ( e.which == 76 ) {
            if ( selCode.replace(/.*(li0.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('LIVE Games');
            } else if ( selCode.replace(/.*(li1.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Finished');
            } else if ( selCode.replace(/.*(li2.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Scheduled');
            } else if ( selCode.replace(/.*(li3.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Odds');
            } else {
                toTab('My Games');
            }
        } else if ( e.which == 72 ) {
            if ( selCode.replace(/.*(li5.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Odds');
            } else if ( selCode.replace(/.*(li4.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Scheduled');
            } else if ( selCode.replace(/.*(li3.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('Finished');
            } else if ( selCode.replace(/.*(li2.*?)".*/, "$1").indexOf('selected') !== -1 ) {
                toTab('LIVE Games');
            } else {
                toTab('All Games');
            }
        }
    }
});
