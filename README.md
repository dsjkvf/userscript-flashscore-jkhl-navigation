FlashScrore: j/k/h/l naigation
==============================

## About

This is a small [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which upon visiting FlashScore site

  - adds a basic Vim-like `j/k` "Scrolling By League" navigation (and on pressing `x` re-reads the offsets table necessary for the navigation);
  - switches between "All Games" / "Live Games" / etc tabs with `h/l` hotkeys;
  - expands / collapses the selected league on `t` key press;
  - expands / collapses all leagues on `q/w` key presses.
